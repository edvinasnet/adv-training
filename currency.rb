# currency class
class Currency
  attr_reader :curr_to, :ratio, :curr_from
  def initialize
    @curr = YAML.load(File.read('ratio.yml'))
  end

  def self.get_calculate_ratio(curr_from, curr_to)
    rat = @curr.find do |current|
      (current.curr_to.to_s == curr_to.to_s) &&
      (current.curr_from.to_s == curr_from.to_s)
    end
    rat.ratio
  end

  def self.load
    @curr = YAML.load(File.read('ratio.yml'))
  end
end
