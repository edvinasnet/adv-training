require_relative 'user'
require_relative 'advert'
require_relative 'Paysera'

require 'yaml'
# Program class
puts 'Program starting'
if File.file?('user.yml')
  @users = User.load_users('user.yml')
  puts 'Useris uzloadinti'
else
  @user = User.new('adverts.yml')
end
name = gets.chomp.to_s
@users.each do |user|
  @user = user if user.user_name == name
end
input = 1
while input != 0
  puts 'Pasirinkite, ka norite daryti: '
  puts '1 - prideti pinigu per paysera'
  puts '2 - prideti skelbima'
  puts '3 - isspausdinti info'
  puts '4 - prideti pinigu per paysera'
  puts '5 - prideti nauja vartotoja'
  puts '6 - rodyti visus skelbimus'
  puts '0 - issaugoti'
  puts 'exit - issaugoti'
  input = gets.chomp.to_i
  if input == 1
    puts ''
    puts 'Iveskite pinigu pridejima:'
    money = gets.chomp
    @user.add_money(money.to_f, 'EUR', Paysera.new)
    puts @user.money

  end
  if input == 2

    puts ''
    puts 'tekstas:'
    text = gets.chomp
    puts 'Pavadinimas:'
    name = gets.chomp
    puts 'Money:'
    money = gets.chomp.to_f
    puts 'Nmae:'
    user_name = gets.chomp

    @user.add_advert(Advert.new(text: text.to_s, currency: 'EUR',
                                create_date: Time.now,
                                comments: [], name: name.to_s,
                                price: money.to_i,
                                user: user_name.to_s, phone: :phone))
    puts ''
  end
  if input == 3
    puts 'Info:'
    puts ''
    @user.adverts.each do |adv|
      puts "#{adv.name}  #{adv.text}  #{adv.name}"
    end
    puts ''
  end
  if input == 4
    puts ''
    puts 'Iveskite pinigu pridejima (PAYPAL):'
    money = gets.chomp
    @user.add_money(money.to_f, 'EUR', Paypal.new)
    puts @user.money

  end
  if input == 5
    puts ''
    puts 'Iveskite vartotojo varda:'
    name = gets.chomp
    user = User.new(name)
    puts user

  end
  if input == 6
    puts ''
    Advert.queue(Advert.all).each_with_index do |advert, _index|
      puts "#{advert.star} #{advert.date} #{advert.name}"
    end
  end
  User.save('user.yml') if input == 0

end
