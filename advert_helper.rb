require 'yaml'
# Advert helper class
class AdvertHelper
  def self.calculate_price(user, _advert)
    config = YAML.load_file('config.conf')
    discount = config['discount']
    if discount['discount'] == true
      return true if user.adverts.size %
                     discount['discount-more'] == 0
    end
    user.debit(config['category']['other-all'].to_f)
  end
end
