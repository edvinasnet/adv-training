require_relative 'currency.rb'

# payment class
class Payment
  def pay(sum, curr)
    ratio = 1
    Currency.load
    ratio = Currency.get_calculate_ratio(curr, 'EUR') if curr != 'EUR'
    (sum * ratio - @vat).round(2)
  end
end
