require 'simplecov'
SimpleCov.start
require 'spec_helper'
describe Advert do
  before :each do
    # args - text, name, price, user, category
    @advert = Advert.new(text: 'Telefonas', currency: 'EUR',
                         create_date: Time.now, comments: [],
                         name: 'Name', price: 10,
                         user: 'Petras', phone: :phone)
    @users = User.load_users('user.yml')
    @curr = Currency.new
  end

  describe 'discount object' do
    it 'returns the correct discount' do
      expect(@advert.make_discount(10)).to eql 9.0
    end
    it 'sorting algorythm' do
      list = []
      @users.each do |user|
        list += user.adverts
      end
      expect(Advert.queue(list)).to queue_checker
    end
    it 'Check time delete advert' do
      list = []
      advert = Advert.new(text: 'Telefonas', currency: 'EUR',
                          create_date: Time.now - (60 * 24 * 60 * 60),
                          name: 'Name', price: 10,
                          user: 'Petras', phone: :phone)
      list.push(advert)
      expect(Advert.check_time(list)).to be_empty
    end
  end

  describe '#comment' do
    it 'returns the correct comment' do
      @advert.allow_comment(true)
      expect(@advert.add_comment('comentaras')).to eq(true)
    end

    it 'disallow comments' do
      expect(@advert.allow_comment(FALSE)).to be false
      expect(@advert.add_comment('comentaras')).to be false
    end

    it 'turn on comments' do
      expect(@advert.allow_comment(TRUE)).to be true
    end
  end

  describe 'double comments' do
    it 'returns the correct comment' do
      @advert.add_comment('comentaras')
      expect(@advert.add_comment('comentaras')).to eq false
    end
  end

  describe 'change expiration date' do
    it 'returns the date changes' do
      expect(@advert.change_expiration_date(30).to_s)
        .to eq((Time.now + (60 * 24 * 60 * 60)).to_s)
    end
    it 'returns the date change when date is back' do
      @advert.change_expiration_date(-100)
      expect(@advert.change_expiration_date(30))
        .to eq false
    end
  end

  describe 'change expiration date' do
    it 'returns the date changes' do
      expect(@advert.change_expiration_date(30).to_s)
        .to eq((Time.now + (60 * 24 * 60 * 60)).to_s)
    end
    it 'returns the date change when date is back' do
      @advert.change_expiration_date(-100)
      expect(@advert.change_expiration_date(30))
        .to eq false
    end
  end

  describe 'currency change price' do
    it 'returns the correct currency change' do
      expect(@advert.currency_change('GBP')).to eql 7.50
    end
    it 'returns the same correct currency change' do
      expect(@advert.currency_change('EUR')).to eql FALSE
    end
    it 'returns the correct price if buy more' do
      expect(@advert.discount_for_more(12)).to eql 9.0
    end
    it 'returns the correct price if buy less' do
      expect(@advert.discount_for_more(1)).to eql 10
    end
  end
end
