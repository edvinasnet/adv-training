require 'rspec/expectations'

RSpec::Matchers.define :file_compare do |expected|
  match do |actual|
    f1 = IO.read(actual)
    f2 = IO.read(expected)
    f1 == f2
  end
end
