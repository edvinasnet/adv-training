require 'rspec/expectations'

RSpec::Matchers.define :max_advert_lib do |adv, times|
  match do |user|
    times.times { user.add_advert(adv) }
  end
end
