require_relative 'advert_lib_spec.rb'
require_relative 'debit_range_spec.rb'
require_relative 'file_compare.rb'
require_relative 'queue_spec.rb'
require_relative '../user.rb'

require_relative '../advert.rb'
require_relative '../currency.rb'

require_relative '../comment.rb'
require_relative '../paysera.rb'
require_relative '../paypal.rb'

require 'yaml'
