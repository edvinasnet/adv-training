require 'simplecov'
require 'json'
SimpleCov.start
require 'spec_helper'
describe 'User object' do
  before :all do
    lib_obj = [
      Advert.new(text: 'kazkoks YOLO', currency: 'EUR',
                 create_date: Time.now, comments: [],
                 name: 'Mobilusis XL',
                 price: 12.90, user: 'Petras', phone: :phone),
      Advert.new(text: 'kazkoks YOLO', currency: 'EUR',
                 create_date: Time.now, comments: [],
                 name: 'Mobilusis XL',
                 price: 12.90, user: 'Petras', phone: :phone),
      Advert.new(text: 'kazkoks YOLO', currency: 'EUR',
                 create_date: Time.now, comments: [],
                 name: 'Mobilusis XL',
                 price: 12.90, user: 'Petras', phone: :phone),
      Advert.new(text: 'kazkoks YOLO', currency: 'EUR',
                 create_date: Time.now, comments: [],
                 name: 'Mobilusis XL',
                 price: 12.90, user: 'Petras', phone: :phone),
      Advert.new(text: 'kazkoks YOLO', currency: 'EUR',
                 create_date: Time.now, comments: [],
                 name: 'Mobilusis XL',
                 price: 12.90, user: 'Petras', phone: :phone)
    ]
    File.open 'adverts.yml', 'w' do |f|
      f.write YAML.dump lib_obj
    end
  end

  before :each do
    @usr = User.new(user_name: 'tester', adverts: [],
                    message: [], money: 0.to_f,
                    adverts_favourite: [])
    @usrert = Advert.new(text: 'kazkoks YOLO', currency: 'EUR',
                         create_date: Time.now, comments: [],
                         name: 'Mobilusis XL',
                         price: 12.90, user: 'Petras', phone: :phone)
    @usr_two = User.new(user_name: 'tester', adverts: [],
                        message: [], money: 0.to_f,
                        adverts_favourite: [])
  end

  describe '#new' do
    context 'with no parameters' do
      it 'has no skelbimai' do
        adv = User.new(user_name: 'tester', adverts: [],
                       message: [], money: 0.to_f,
                       adverts_favourite: [])
        expect(adv.adverts).to be_empty
      end
    end
    context 'with a yaml file parameter' do
      it 'has five skelbimai' do
        @usr.load_adverts('skelbimai.yml')
        expect(@usr.adverts.size).to be 5
      end
    end
  end

  it '#adverts new raise error' do
    obj = Advert.new(text: 'kazkoks YOLO', currency: 'EUR',
                     create_date: Time.now, comments: [],
                     name: 'Mobilusis XL',
                     price: 12.90, user: 'Petras', phone: :phone)
    @usr.add_advert(obj)
    expect { @usr.add_advert(obj) }.to raise_error 'No much money'
  end

  it '#add money' do
    @usr.add_money(10, 'GBP', Paysera.new)
    expect(@usr.money).to eql 13.65
  end

  it '#adverts new' do
    obj = Advert.new(text: 'Mobilusis X', currency: 'EUR',
                     create_date: Time.now, comments: [],
                     name: 'Mobilusis X',
                     price: 12.90, user: 'Petras', phone: :phone)

    @usr.add_money(10, 'EUR', Paysera.new)
    @usr.add_advert(obj)
    advert = @usr.get_advert('Mobilusis X')
    expect(advert).to eql([obj])
  end

  it '#adverts with other category' do
    obj = Advert.new(text: 'kazkoks YOLO', currency: 'EUR',
                     create_date: Time.now, comments: [],
                     name: 'Mobilusis XL',
                     price: 12.90, user: 'Petras', phone: :phone)

    @usr.add_money(10, 'EUR', Paysera.new)
    @usr.add_advert(obj)
    advert = @usr.get_advert('Mobilusis XL')
    expect(advert).to eql([obj])
  end

  describe '#favourite adverts' do
    it 'add to favourite one advert' do
      expect(@usr.add_to_favourite(@usrert)).to be_truthy
    end

    it 'add to favourite two adverts' do
      expect(@usr.add_to_favourite(@usrert)).to be_truthy
      expect(@usr.add_to_favourite(@usrert)).to be_falsey
    end

    it 'check favourite' do
      @usr.add_to_favourite(@usrert)
      expect(@usr.adverts_favourite).to include(@usrert)
    end
  end

  it '#max advert' do
    obj = Advert.new(text: 'kazkoks YOLO', currency: 'EUR',
                     create_date: Time.now, comments: [], name: 'Mobilusis X',
                     price: 12.90, user: 'Petras', phone: :phone)

    @usr.add_money(10, 'EUR', Paysera.new)
    expect(@usr).to max_advert_lib(obj, 5)
  end

  describe 'file savings' do
    it 'saves the user' do
      user = User.new(user_name: 'tester', adverts: [],
                      message: [], money: 0.to_f,
                      adverts_favourite: [])
      User.add(user)
      User.save('user-test-temp.yml')
      expect('user-test-temp.yml').to file_compare 'user-test.yml'
    end
  end

  describe 'file loading' do
    it 'load the users file' do
      users = []
      users.push(User.new(user_name: 'tester', adverts: [],
                          message: [], money: 0.to_f,
                          adverts_favourite: []))
      expect(users.to_yaml).to eq(User.load_users('user-test.yml').to_yaml)
    end
  end
  describe '#payments' do
    it '#paysera add' do
      expect(@usr.add_money(10, 'EUR', Paysera.new)).to be 9.75
    end

    it '#paysera add' do
      expect(@usr.add_money(10, 'EUR', Paypal.new)).to be 9.0
    end

    it '#send money to user' do
      @usr.add_money(10.25, 'EUR', Paysera.new)
      expect(@usr.send_money(5, @usr_two)).to debit_range(5, 10)
    end
  end
end
