require 'rspec/expectations'

RSpec::Matchers.define :queue_checker do
  match do |queue|
    queue.each_with_index do |adv, index|
      return true if index == queue.size - 1
      return false if (adv.date <= Time.now)
      return false if adv.star.to_i < queue[index + 1].star.to_i
    end
  end
end
