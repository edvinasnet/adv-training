require 'simplecov'
SimpleCov.start
require 'spec_helper'
describe Comment do
  before :each do
    @comment = Comment.new('Komentaras', 'Jonas')
  end

  describe '#ratings' do
    it 'add comments rating' do
      @comment.up_rating(1)
      expect(@comment.rating).to eq 1
    end

    it 'down comments rating' do
      @comment.down_rating(1)
      expect(@comment.rating).to be < 0
    end
  end
end
