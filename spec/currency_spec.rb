require 'simplecov'
SimpleCov.start
require 'spec_helper'
describe Currency do
  before :each do
    Currency.load
    @curr = Currency
  end

  describe '#discount' do
    it 'returns the correct discount' do
      Currency.load
      expect(Currency.get_calculate_ratio('EUR', 'GBP')).to eql 0.75
    end
  end
end
