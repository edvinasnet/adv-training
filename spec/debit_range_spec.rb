require 'rspec/expectations'

RSpec::Matchers.define :debit_range do |debit, sum|
  match do |get|
    get + debit == sum
  end
end
