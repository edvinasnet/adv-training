require 'yaml'
# file: advert_price.rb
require_relative 'Advert_helper'
require_relative 'User_payment'

# there is user class
class User
  @collection = []
  attr_reader :adverts, :user_name, :adverts_favourite, :message, :money
  # args - user_name, adverts, message, money, adverts_favourite
  def initialize(params = {})
    params.each { |key, value| instance_variable_set("@#{key}", value) }
  end

  def load_adverts(adv_file)
    @adv_file = adv_file if adv_file != false
    @adverts = @adv_file ? YAML.load(File.read(@adv_file)) : nil
  end

  def add_to_favourite(adv)
    if !@adverts_favourite.include?(adv)
      @adverts_favourite.push(adv)
      @adverts_favourite
    else
      false
    end
  end

  def add_dir_money(money)
    @money += money if @money < 100
  end

  def add_advert(adv)
    @adverts.push adv if AdvertHelper.calculate_price(self, adv)
  end

  def add_money(money, curr, payment)
    @money += payment.pay(money, curr).to_f
  end

  def get_advert(title)
    @adverts.select { |advert| advert.name.upcase == title.upcase }
  end

  def send_money(sum, user)
    debit = (UserPayment.new).pay(sum, 'EUR', self)
    user.add_dir_money(debit) if debit != FALSE
  end

  def self.all
    @collection
  end

  def self.add(user)
    @collection.push(user)
  end

  def debit(money)
    if money <= @money
      @money -= money
      return true
    else
      fail 'No much money'
    end
  end

  def self.save(user_file = 'user.yml')
    File.open user_file, 'w' do |file|
      file.write YAML.dump User.all
    end
  end

  def self.load_users(user_file)
    YAML.load(File.read(user_file))
  end
end
