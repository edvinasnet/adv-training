require_relative 'Payment'

# payser payment class
class UserPayment < Payment
  attr_reader :vat
  def initialize
    @vat = 0
  end

  def pay(sum, curr, usr_debt)
    super(sum, curr) if @vat == 0 && usr_debt.debit(sum)
  end
end
