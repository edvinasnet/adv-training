# comment class
class Comment
  attr_reader :comment, :user, :rating
  def initialize(comment, user)
    @comment = comment
    @user = user
    @rating = 0
  end

  def up_rating(value)
    @rating += value
  end

  def down_rating(value)
    @rating -= value
  end
end
