require_relative 'currency.rb'
require_relative 'Advert_helper'

# advert class
class Advert
  include Comparable
  attr_reader :text, :name, :price, :user, :comments, :category, :date,
              :allow_comments, :star
  # args - text, name, price, user, category
  def initialize(params = {})
    params.each { |key, value| instance_variable_set("@#{key}", value) }
    @date = @create_date + (30 * 24 * 60 * 60)
  end

  def make_discount(percentage)
    @price -= @price * (percentage.to_f / 100)
  end

  def add_comment(comment)
    if can_add_comment(comment)
      @comments.push(comment)
      return TRUE
    else
      return FALSE
    end
  end

  def self.queue(list)
    list = list.sort_by(&:star).reverse!
    Advert.check_time(list)
  end

  def self.check_time(list)
    list.delete_if { |advert| advert.date < Time.now }
  end

  def allow_comment(value)
    if value == FALSE
      @allow_comments = @comments.size == 0 ? value : @allow_comments
    else
      @allow_comments = value
    end
  end

  def can_add_comment(comment)
    if @comments.empty?
      return FALSE if @comments.any? { |com| com == comment.comment }
    end
    @allow_comments
  end

  def change_expiration_date(days)
    if @date < Time.now
      return FALSE
    else
      @date += (days.to_i * 24 * 60 * 60)
    end
  end

  def currency_change(curr)
    if @currency != curr
      Currency.load
      @price *= Currency.get_calculate_ratio(@currency, curr)
      @price.round(2)
    else
      FALSE
    end
  end

  def discount_for_more(unit)
    if unit > 10
      (@price * 0.9).round(2)
    else
      @price
    end
  end
end
